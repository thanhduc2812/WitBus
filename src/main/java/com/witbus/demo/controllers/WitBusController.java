package com.witbus.demo.controllers;

import com.witbus.demo.dao.models.Bus;
import com.witbus.demo.dto.BusDTO;
import com.witbus.demo.dto.Bus_OwnerDTO;
import com.witbus.demo.dto.SeatDTO;
import com.witbus.demo.services.WitBusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class WitBusController {
    @Autowired
    private WitBusService witBusService;

    @GetMapping(value = "/bus")
    public @ResponseBody
    List<BusDTO> getBus(){
        return witBusService.listBus();
    }

    @PostMapping(value = "/bus")
    public @ResponseBody String addBus(@RequestBody BusDTO busDTO){
        witBusService.addBus(busDTO);
        return "Ok";
    }

    @GetMapping(value = "/bus_owner")
    public @ResponseBody
    List<Bus_OwnerDTO> getBusOwner(){
        return witBusService.listBusOwner();
    }

    @PostMapping(value = "/bus_owner")
    public @ResponseBody String addBusOwner(@RequestBody Bus_OwnerDTO bus_ownerDTO){
        witBusService.addBusOwner(bus_ownerDTO);
        return "Ok";
    }

    @GetMapping(value = "/seat")
    public @ResponseBody
    List<SeatDTO> getSeat(){
        return witBusService.listSeat();
    }

    @PostMapping(value = "/seat")
    public @ResponseBody String addBusOwner(@RequestBody SeatDTO seatDTO){
        witBusService.addSeat(seatDTO);
        return "Ok";
    }
}
