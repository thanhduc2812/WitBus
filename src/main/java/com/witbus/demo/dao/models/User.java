package com.witbus.demo.dao.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "users")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long id;

    @Column(name = "user_name")
    private String username;

    @Column(name = "user_phone")
    private String userphone;

    @Column(name = "user_email")
    private String useremail;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private Set<Booking> bookings;
}
